package br.com.ms.dtoexample.model;

import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@NoArgsConstructor // JPA Only
@Table(name = "tb_usuario")
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "nome", length = 3)
    @NotNull(message = "Name cannot be null")
    @Min(value = 18, message = "Name should not be less than 18")
    @Max(value = 150, message = "Name should not be greater than 150")
    @NotBlank
 
    private String nome;

    @NotNull(message = "Email cannot be null")
    @Email(message = "Email should be valid")
    @NotBlank
    @Column(name = "email")
    private String email;

    @NotBlank
    @NotNull(message = "senha cannot be null")
    @Column(name = "senha")
    private String senha;

    @NotNull(message = "admin cannot be null")
    @Column(name = "admin")
    private boolean admin = false;

    public Usuario(String nome, String email, String senha) {
        this.nome = nome;
        this.email = email;
        this.senha = senha;
    }
}

